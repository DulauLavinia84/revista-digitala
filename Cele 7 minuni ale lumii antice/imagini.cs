﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Cele_7_minuni_ale_lumii_antice
{
    public partial class imagini : Form
    {
        public int contor;
        string[] MINUNI = new string[8];

        public imagini()
        {
            InitializeComponent();

            StreamReader file;
            file = new StreamReader(@"..\..\Resources\CUPRINS.txt");
            for (int k = 1; k <= 7; k++)
                MINUNI[k] = file.ReadLine();

            aplica_minune(1);
        }

        private void aplica_minune(int caseSwitch)
        {
            textBox1.Text = MINUNI[caseSwitch];
            switch (caseSwitch)
            {
                case 1:
                    minune1();
                    break;
                case 2:
                    minune2();
                    break;
                case 3:
                    minune3();
                    break;
                case 4:
                    minune4();
                    break;
                case 5:
                    minune5();
                    break;
                case 6:
                    minune6();
                    break;
                case 7:
                    minune7();
                    break;
                default:
                    minune1();
                    break;
            }
        }

        private void minune1()
        {
            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.pk1;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox2.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.pk2;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox3.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.pk3;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox4.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.pk4;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;

            contor = 1;
        }

        private void minune2()
        {
            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.gs1;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox2.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.gs2;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox3.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.gs3;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox4.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.gs4;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 2;
        }

        private void minune3()
        {
            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.tz1;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox2.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.tz2;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox3.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.tz3;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox4.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.tz4;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 3;
        }

        private void minune4()
        {
            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.stz1;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox2.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.stz2;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox3.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.stz3;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox4.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.stz4;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;

            contor = 4;
        }

        private void minune5()
        {
            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.mh1;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox2.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.mh2;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox3.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.mh3;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox4.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.mh4;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;

            contor = 5;
        }

        private void minune6()
        {
            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.cr1;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox2.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.cr2;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox3.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.cr3;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox4.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.cr4;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;

            contor = 6;
        }

        private void minune7()
        {
            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.fa1;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox2.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.fa2;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox3.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.fa3;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBox4.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.fa4;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;

            contor = 7;
        }

        private void back_Click(object sender, EventArgs e)
        {
            contor--;
            if (contor == 0)
                contor = 7;
            aplica_minune(contor);
        }

        private void next_Click(object sender, EventArgs e)
        {

            contor++;
            if (contor == 8)
                contor = 1;
            aplica_minune(contor);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
