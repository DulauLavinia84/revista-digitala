﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Cele_7_minuni_ale_lumii_antice
{
    public partial class minuni : Form
    {
        public int contor = -1;

        public minuni()
        {
            InitializeComponent();
            cuprins();
        }

        private void cuprins ()
        {
            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.g2;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\CUPRINS.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }
            contor = -1;
            textBox1.Text = "-1";
        }

        private void aplica_minune (int caseSwitch)
        {
            textBox1.Text = Convert.ToString(contor);
            switch (caseSwitch)
            {
                case -1:
                    cuprins();
                    break;
                case 0:
                    pg();
                    break;
                case 1:
                    minune1();
                    break;
                case 2:
                    minune2();
                    break;
                case 3:
                    minune3();
                    break;
                case 4:
                    minune4();
                    break;
                case 5:
                    minune5();
                    break;
                case 6:
                    minune6();
                    break;
                case 7:
                    minune7();
                    break;
                default:
                    cuprins();
                    break;
            }
        }

        private void pg ()
        {
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\pg.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }

            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.g3;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 0;
        }

        private void minune1 ()
        {
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\minune1.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }

            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.pk;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 1;
        }

        private void minune2()
        {
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\minune2.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }

            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.gs;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 2;
        }

        private void minune3()
        {
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\minune3.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }

            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.tz;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 3;
        }

        private void minune4()
        {
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\minune4.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }

            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.stz;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 4;
        }

        private void minune5()
        {
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\minune5.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }

            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.mh;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            contor =5;
        }

        private void minune6()
        {
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\minune6.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }

            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.cr;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 6;
        }

        private void minune7()
        {
            richTextBox1.Text = "";
            StreamReader file;
            file = new StreamReader(@"..\..\Resources\minune7.txt");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                richTextBox1.Text = richTextBox1.Text + line + '\n';
            }

            pictureBox1.BackgroundImage = Cele_7_minuni_ale_lumii_antice.Properties.Resources.fa;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            contor = 7;
        }

        private void NEXT_Click(object sender, EventArgs e)
        {
            contor++;
            if (contor == 8)
                contor = -1;
            aplica_minune(contor);
        }

        private void BACK_Click(object sender, EventArgs e)
        {
            contor--;
            if (contor == -2)
                contor = 7;
            aplica_minune(contor);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            minune1();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            minune2();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            minune3();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cuprins();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            pg();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            minune4();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            minune5();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            minune6();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            minune7();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
