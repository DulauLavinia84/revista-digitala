﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cele_7_minuni_ale_lumii_antice
{
    public partial class acasa : Form
    {
        public acasa()
        {
            InitializeComponent();
        }

        private void inapoi_acasa_Click(object sender, EventArgs e)
        {
            despre f = new despre();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            minuni f = new minuni();
            f.Show();
        }

        private void acasa_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            imagini f = new imagini();
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            video f = new video();
            f.Show();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            QUIZ2 f = new QUIZ2();
            f.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
