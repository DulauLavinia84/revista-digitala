﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cele_7_minuni_ale_lumii_antice
{
    public partial class video : Form
    {
        public int contor;

        public video()
        {
            InitializeComponent();
            axWindowsMediaPlayer1.settings.autoStart = false;//pentru a nu porni muzica automat 
            aplica_minune(1);
        }

        private void aplica_minune(int caseSwitch)
        {
            switch (caseSwitch)
            {

                case 1:
                    {
                        axWindowsMediaPlayer1.URL = @"1.mp4";
                        contor = 1;
                    }
                    break;
                case 2:
                    {
                        axWindowsMediaPlayer1.URL = @"2.mp4";
                        contor = 2;
                    }
                    break;
            }
        }

        private void NEXT_Click(object sender, EventArgs e)
        {
            contor++;
            if (contor == 3)
                contor = 1;
            aplica_minune(contor);
        }

        private void BACK_Click(object sender, EventArgs e)
        {
            contor--;
            if (contor == 0)
                contor = 2;
            aplica_minune(contor);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
