﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Cele_7_minuni_ale_lumii_antice
{
    public partial class QUIZ2 : Form
    {
        StreamReader file=new StreamReader(@"..\..\Resources\quiz.txt");
        string intrebare, r1, r2, r3, rc;
        int nota = 3;
        int ct = 0;

        private void QUIZ2_Activated(object sender, EventArgs e)
        {
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        bool sem = false;

        private void button1_Click(object sender, EventArgs e)
        {
            if (sem == true)
                MessageBox.Show("Ai terminat deja acest quiz.");
            else
            {
                if (radioButton1.Checked == false && radioButton2.Checked == false && radioButton3.Checked == false)
                    MessageBox.Show("Raspunde la intrebare!");
                else
                {
                    if ((radioButton1.Checked == true && r1 == rc) || (radioButton2.Checked == true && r2 == rc) || (radioButton3.Checked == true && r3 == rc))
                    {
                        nota++;
                    }
                    if (ct < 7)
                        citesc();
                    else
                    {
                        MessageBox.Show("Ai luat nota " + Convert.ToString(nota) + ".");
                        sem = true;
                    }

                    radioButton1.Checked = false;
                    radioButton2.Checked = false;
                    radioButton3.Checked = false;
                }
            }
        }

        void citesc ()
        {
            intrebare = file.ReadLine();
            r1 = file.ReadLine();
            r2 = file.ReadLine();
            r3 = file.ReadLine();
            rc = file.ReadLine();

            label1.Text = intrebare;
            radioButton1.Text = r1;
            radioButton2.Text = r2;
            radioButton3.Text = r3;

            ct++;
        }

        public QUIZ2()
        {
            InitializeComponent();
            citesc();
        }
    }
}
